CREATE DATABASE capstone_project;
use capstone_project;



CREATE TABLE `Users` (
  `id` INT,  -- should I use varchar and store uuid values?--
  `user_name` VARCHAR(250),
  `password` VARCHAR(250),
  `email_address` VARCHAR(250),
  `first_name` VARCHAR(250),
  `last_name` VARCHAR(250),
  `role` VARCHAR(250),
  PRIMARY KEY (`id`)
);

CREATE TABLE `Tests` (
  `id` INT, -- should I use varchar and store uuid values?--
  `name` VARCHAR(250),
  PRIMARY KEY `Key` (`id`)
);

CREATE TABLE `Questions` (
   `id` INT, -- should I use varchar and store uuid values?--
  `title` VARCHAR(250),
  `description` VARCHAR(250),
  `answer_key` VARCHAR(250),
  `test_id`  INT,
  FOREIGN KEY (`test_id`) REFERENCES `Tests`(`id`),
  KEY `Key` (`title`, `description`, `answer_key`, `id`),
  KEY `foreign` (`test_id`)
);

CREATE TABLE `Users_Questions` (
  `user_id` INT,
  `question_id` INT,
  `score` INT, -- Or should it be varchar to store as string expressing percentages? --
  FOREIGN KEY (`user_id`) REFERENCES `Users`(`id`),
  KEY `Key` (`user_id`, `question_id`, `score`)
);

CREATE TABLE `Users_Tests` (
  `user_id` INT,
  `test_id` INT,
  FOREIGN KEY (`test_id`) REFERENCES `Tests`(`id`),
  FOREIGN KEY (`user_id`) REFERENCES `Users`(`id`),
  KEY `Key` (`user_id`, `test_id`)
);

