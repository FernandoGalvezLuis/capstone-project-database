CREATE TABLE `Users` (
  `id` bigint,
  `user_name` Type,
  `password` Type,
  `email_address` Type,
  `first_name` Type,
  `last_name` Type,
  `role` Type,
  KEY `primary` (`id`)
);

CREATE TABLE `Tests` (
  `id` Type,
  `Field` Type,
  `Field` Type,
  KEY `Key` (`id`, `Field`, `Field`)
);

CREATE TABLE `Questions` (
  `title` Type,
  `description` Type,
  `answer_key` Type,
  `id` Type,
  `test_id` Type,
  FOREIGN KEY (`test_id`) REFERENCES `Tests`(`id`),
  KEY `Key` (`title`, `description`, `answer_key`, `id`),
  KEY `foreign` (`test_id`)
);

CREATE TABLE `Users_Questions` (
  `user_id` Type,
  `question_id` Type,
  `score` Type,
  FOREIGN KEY (`user_id`) REFERENCES `Users`(`id`),
  KEY `Key` (`user_id`, `question_id`, `score`)
);

CREATE TABLE `Users_Tests` (
  `user_id` Type,
  `test_id` Type,
  FOREIGN KEY (`test_id`) REFERENCES `Tests`(`id`),
  FOREIGN KEY (`user_id`) REFERENCES `Users`(`id`),
  KEY `Key` (`user_id`, `test_id`)
);

